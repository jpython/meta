# Liste des projets / List of projects

Note : certains projets ne sont pas publics. S'agissant des formations elles sont rendue accessibles
aux participants qui les ont suivies et qui le souhaitent.

This page is still updated, anyway it has been moved to : https://codeberg.org/jpm/meta

## Einstein-Poincaré synchronization

Client side Python Web programming : 

- https://noedge.net/e/
- https://codeberg.org/jpm/pyweb-einstein

## Introduction aux bases de données relationnelles et à SQL avec MySQL/MariaDB et PostgreSQL

https://gitlab.com/python_431/mypostsql-course-fr

## Python for data analysis: numpy, pandas, matplotlib (public access)

https://framagit.org/jpython/pydata

## Photoshoot: how to structure an application as a python package [in progress]

https://gitlab.com/python_431/photoshoot

## Mathématiques pour l'informatique (accès public)

https://framagit.org/jpython/math

## Histoire et culture de l'informatique (accès public)

https://framagit.org/jpython/culthistinfo

## Déploiement AWS avec Terraform (accès public)

https://framagit.org/jpython/aws-terraform-1

## Atelier Linux temps réel (accès public)

https://framagit.org/jpython/linuxrt

## Atelier Elastic Suite -- Search/Kibana/etc. (accès public)

https://framagit.org/jpython/ipssi-elastic

## Atelier PostgreSQL (accès public)

https://framagit.org/jpython/cours-posgresql-1

https://framagit.org/jpython/cours-postgresql-2

## Mise en œuvre d'un cluster Elastic Suite (Elastic Search/Kibana) dans le cloud AWS avec Terraform (accès public)

https://framagit.org/jpython/formation-elk

## ShareCode: A codeshare.io like Python Web Application with Flask (public access)

https://framagit.org/jpython/share-code-plus

https://framagit.org/jpython/share-code

## Introduction to Python for data science (access for students)

https://framagit.org/jpython/python-course

## Python Object Oriented Programming (access for students)

https://framagit.org/jpython/python-training-adv

## Miscellaneous Python scripts and modules (public access)

https://framagit.org/jpython/miscellaneous-python

## Introduction à la programmation en Python (accès réservé)

Base de la programmation et de l'algorithmique avec exercices en Python 3.

https://framagit.org/jpython/introduction-programmation-en-python

## Crible d'Érathostène, Python, Cython, C, ... (accès public)

Comparaisons de performances d'implémentations du crible d'Érathostène sous forme de modules
Python.

https://framagit.org/jpython/sieve_profile

## Python - une introduction complète (accès réservé)

Introduction complète au langage Python et au développement objet pour développeurs.

https://framagit.org/jpython/formation-python-1

## chti : projet dans le cadre d'une formation au Cloud Computing (accès public)

https://framagit.org/jpython/chti

## chti-solution : point d'étape avec proposition de solution (accès public)

https://framagit.org/jpython/chti-solution

## Snippets

- Switch.py: https://framagit.org/snippets/4544 (accès public)
